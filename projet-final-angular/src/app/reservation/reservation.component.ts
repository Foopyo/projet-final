import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Client } from '../client';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

  nomresa: string;
  telresa: string;
  nbrresa: number;
  jour1: string;
  heure1: string;
  listJour = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"];
  listHeur = ["midi", "soir"];
  message_resa2: string;
  cli3: Client;

  newresa = { jour: '', heure: '', nbrpers: 0, nom: '', tel: '', idclient: 0, jourNumber: 0, heureNumber: 0 };

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.cli3 = JSON.parse(sessionStorage.getItem("clico"));
    this.nbrresa = 1;
    if (this.cli3 != null) {
      this.nomresa = this.cli3.nom;
      this.telresa = this.cli3.tel;
    }
  }

  valideresa() {
    if (this.jour1 == null || this.heure1 == null || this.nomresa == null || this.telresa == null) {
      this.message_resa2 = "Erreur de saisie, veuillez remplir tous les champs";
    }
    else if(this.nbrresa < 1){
      this.message_resa2 = "La réservation doit être pour au moins une personne";
    }
    else {
      this.newresa.jour = this.jour1;
      this.newresa.heure = this.heure1;
      this.newresa.nom = this.nomresa;
      this.newresa.tel = this.telresa;
      this.newresa.nbrpers = this.nbrresa;
      //il faut que je récupère l'id dans le session storage préalablement
      this.newresa.idclient = this.cli3 != null ? this.cli3.id : -1;
      this.newresa.jourNumber = this.listJour.indexOf(this.newresa.jour);
      this.newresa.heureNumber = this.newresa.heure == "midi" ? 0 : 1;
      const body = JSON.stringify(this.newresa);

      this.http.post("http://localhost:8080/projetFinal/reservations/", body, {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        })
      }).subscribe(
        response => {

          this.message_resa2 = "Votre réservation a bien été prise en compte";



        }
        ,
        err => {
          this.message_resa2 = "Oups, un problème est intervenu dans la réservation de votre table"
        }
      );

    }

  }





}
