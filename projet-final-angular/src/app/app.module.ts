import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AppHelloComponent } from './app-hello/app-hello.component';
import { ConnectionclientComponent } from './connectionclient/connectionclient.component';
import { InscriptionclientComponent } from './inscriptionclient/inscriptionclient.component';
import { Inscriptionclient2Component } from './inscriptionclient2/inscriptionclient2.component';
import { PageutilisateurComponent } from './pageutilisateur/pageutilisateur.component';
import { DeconnectionComponent } from './deconnection/deconnection.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommandeChoixComponent } from './commande-choix/commande-choix.component';
import { CommandeRecapComponent } from './commande-recap/commande-recap.component';
import { CommandeInfoComponent } from './commande-info/commande-info.component';
import { MenuComponent } from './menu/menu.component';
import { AccueilComponent } from './accueil/accueil.component';
import { CarteComponent } from './carte/carte.component';
import { AdminlistartComponent } from './adminlistart/adminlistart.component';
import { ReservationComponent } from './reservation/reservation.component';
import { AdminresaComponent } from './adminresa/adminresa.component';
import { HistoriquecommandeComponent } from './historiquecommande/historiquecommande.component';
import { ResaClientComponent } from './resa-client/resa-client.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AppHelloComponent,
    CommandeChoixComponent,
    CommandeRecapComponent,
    CommandeInfoComponent,
    MenuComponent,
    AccueilComponent,
    CarteComponent,
    AppHelloComponent,
    ConnectionclientComponent,
    InscriptionclientComponent,
    Inscriptionclient2Component,
    PageutilisateurComponent,
    DeconnectionComponent,
    AdminlistartComponent,
    ReservationComponent,
    AdminresaComponent,
    HistoriquecommandeComponent,
    ResaClientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
