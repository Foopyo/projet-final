import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from '../article';
import { SrvCrudService } from '../srv-crud.service';

@Component({
  selector: 'app-carte',
  templateUrl: './carte.component.html',
  styleUrls: ['./carte.component.css']
})
export class CarteComponent implements OnInit {

  Mylist: Array<Article>;

  constructor(private srv: SrvCrudService) { }

  ngOnInit(): void {
    this.srv.getListeArticle();
    this.Mylist = JSON.parse(sessionStorage.getItem("liste_article"));
  }

}
