import { Component, OnInit } from '@angular/core';
import { Ligne } from '../ligne';
import { Router } from '@angular/router';
import { Commande } from '../commande';
import { SrvCrudService } from '../srv-crud.service';

@Component({
  selector: 'app-commande-recap',
  templateUrl: './commande-recap.component.html',
  styleUrls: ['./commande-recap.component.css']
})
export class CommandeRecapComponent implements OnInit {

  liste_commande:Array<Ligne>;
  prix_commande:number;

  constructor(private route: Router, private srv: SrvCrudService) { }

  ngOnInit(): void {
    let c: Commande = new Commande();
    c=JSON.parse(sessionStorage.getItem("commande"));

    this.liste_commande=c.listLigne;
    this.prix_commande=c.prixGlobal;
  }

  retour(){
    this.route.navigate(['commandechoix']);
  }

  valider(){

    let date = this.formatDate(new Date());

    this.srv.createCommande(date);
 
    this.route.navigate(['commandeinfo']);
  }

  padTo2Digits(num: number) {
    return num.toString().padStart(2, '0');
  }

  //'yyyy-MM-dd'T'HH:mm:ss.SSSZ'
  formatDate(date: Date) {
    return (
      [
        date.getFullYear(),
        this.padTo2Digits(date.getMonth() + 1),
        this.padTo2Digits(date.getDate()),
      ].join('-') +
      'T' +
      [
        this.padTo2Digits(date.getHours()),
        this.padTo2Digits(date.getMinutes()),
        this.padTo2Digits(date.getSeconds()),
        
      ].join(':') +
      '.' +
      [
        this.padTo2Digits(date.getMilliseconds()),
      ] +
      'Z'
    );
  }
}
