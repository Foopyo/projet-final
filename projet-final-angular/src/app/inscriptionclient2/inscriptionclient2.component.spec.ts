import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Inscriptionclient2Component } from './inscriptionclient2.component';

describe('Inscriptionclient2Component', () => {
  let component: Inscriptionclient2Component;
  let fixture: ComponentFixture<Inscriptionclient2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Inscriptionclient2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Inscriptionclient2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
