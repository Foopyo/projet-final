import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Client } from '../client';
import { LoginService } from '../login-service';

@Component({
  selector: 'app-inscriptionclient2',
  templateUrl: './inscriptionclient2.component.html',
  styleUrls: ['./inscriptionclient2.component.css']
})
export class Inscriptionclient2Component implements OnInit {


  // cli11:Client;
  newcli = { login: '', psw: '', nom: '', prenom: '', adresse: '', tel: '' };
  cli11 = { id: 0, login: '', psw: '', nom: '', prenom: '', adresse: '', tel: '' };
  //newcli= new Client();

  //newcli2={login:'',psw:'',nom:'',prenom:'',adresse:'',tel:''};



  message_form_inscr2: string;

  constructor(private http : HttpClient,private router : Router, private logServ:LoginService) { }

  ngOnInit(): void {

    this.newcli.login = sessionStorage.getItem("loginscr");

    //redirection si on ne vient pas de la page précédente:
    if (this.newcli.login == null) {
      this.router.navigate(['/inscriptionclient']);
    }



  }

  verif_form_inscr() {

    if (this.newcli.login == '' || this.newcli.nom == '' || this.newcli.prenom == '' || this.newcli.psw == '' || this.newcli.adresse == '' || this.newcli.tel == '') {
      this.message_form_inscr2 = "Erreur de saisie, veuillez remplir tous les champs";
    }
    else {

      // this.newcli2.login=this.newcli.login;
      //  this.newcli2.psw=this.newcli.psw;
      //  this.newcli2.nom=this.newcli.nom;
      // this.newcli2.prenom=this.newcli.prenom;
      // this.newcli2.adresse=this.newcli.adresse;
      // this.newcli2.tel=this.newcli.tel;

      // const body=JSON.stringify(this.newcli2);

      const body = JSON.stringify(this.newcli);

      this.http.post("http://localhost:8080/projetFinal/clients/", body, {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        })
      }).subscribe(
        response => {


          console.log("avantfindbylog");
          console.log(this.newcli.login);

          this.findbylog(this.newcli.login);
          //this.cli1.id=this.cli11.id;

          // console.log("aprèsfindbylog");

          // console.log(sessionStorage.getItem("clico"));
          // console.log(this.cli11.id);

          // let str:string = JSON.stringify(this.newcli);

          // let str:string = JSON.stringify(this.cli11);
          // sessionStorage.setItem("clico",str);

          //  console.log(str);




          // sessionStorage.removeItem("loginscr");
          //   this.router.navigate(['/pageutilisateur']);

        }
        ,
        err => {
          this.message_form_inscr2 = "Oups, un problème est intervenu dans la création de votre compte"
        }
      );

    }


  }

  findbylog(logi) {

    this.http.get<Client>("http://localhost:8080/projetFinal/clients/searchLogin/" + logi).subscribe(
      response => {
        // this.cli11=response;
        sessionStorage.setItem("clico", JSON.stringify(response));

        console.log(sessionStorage.getItem("clico"));
        sessionStorage.removeItem("loginscr");
        this.logServ.sendUpdate();
      ;
      }
      ,
      err => {

      }
    );

  }

}
