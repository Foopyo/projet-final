import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriquecommandeComponent } from './historiquecommande.component';

describe('HistoriquecommandeComponent', () => {
  let component: HistoriquecommandeComponent;
  let fixture: ComponentFixture<HistoriquecommandeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoriquecommandeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistoriquecommandeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
