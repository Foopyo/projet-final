import { Component, OnInit } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Client } from '../client';
import { Commande2 } from '../commande2';
import { Ligne } from '../ligne';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-historiquecommande',
  templateUrl: './historiquecommande.component.html',
  styleUrls: ['./historiquecommande.component.css'],
  providers: [DatePipe]
})
export class HistoriquecommandeComponent implements OnInit {

  lstcom: Array<Commande2>;
cli1: Client;
pr:number;
pr2:number;
lstli:Array<Ligne>;


  constructor(private http: HttpClient, public datePipe:DatePipe) { }

  ngOnInit(): void {
    this.init();
    this.findcommandes();
  }

init(){
  this.cli1 = JSON.parse(sessionStorage.getItem("clico"));
}

  findcommandes(){

    const body = JSON.stringify(this.cli1);

    this.http.post<Array<Commande2>>("http://localhost:8080/projetFinal/commandes/searchByClient", body,{

      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(
      response => {
        this.lstcom=response;
        this.recupprixcom();

      }
      ,
      err => {
       
      }
    );

  }

  recupprixcom(){
    console.log("dans recup");
    this.lstcom.forEach(ll=>{
      this.pr=0;
      this.lstli=ll.lignes;

      console.log("dans recup2");
      this.lstli.forEach(aa=>{
        this.pr=this.pr+aa.article.prix*aa.quantite;
        console.log(aa.article.prix);
        console.log(aa.quantite);
        
      });
      console.log(this.pr);
ll.prixGlobal=this.pr;
    });

    
  }
}
