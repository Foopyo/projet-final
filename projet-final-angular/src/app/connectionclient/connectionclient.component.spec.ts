import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionclientComponent } from './connectionclient.component';

describe('ConnectionclientComponent', () => {
  let component: ConnectionclientComponent;
  let fixture: ComponentFixture<ConnectionclientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnectionclientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConnectionclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
