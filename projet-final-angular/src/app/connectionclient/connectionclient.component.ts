import { Component, OnInit } from '@angular/core';
import { Client } from '../client';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { LoginService } from '../login-service';

@Component({
  selector: 'app-connectionclient',
  templateUrl: './connectionclient.component.html',
  styleUrls: ['./connectionclient.component.css']
})
export class ConnectionclientComponent implements OnInit {

  log1: string;
  mdp1: string;

  cli1: Client;

  message_co: string;


  constructor(private http: HttpClient, private router: Router, private logServ:LoginService) { }

  ngOnInit(): void {
    //ici on redirige vers la page utilisateur si on est déjà connecté:
    this.cli1 = JSON.parse(sessionStorage.getItem("clico"));

    if (this.cli1 != null) {
      this.router.navigate(['/pageutilisateur']);
    }
  }

  //problème ici, ne passe pas dans l'erreur en cas de mauvais mdp
  connect() {
    this.http.get<Client>("http://localhost:8080/projetFinal/clients/" + this.log1 + "/" + this.mdp1).subscribe(
      response => {
        if (response == null) {
          this.message_co = "Mauvais login et/ou mot de passe";
        }
        else {
          this.message_co = "";

          let str: string = JSON.stringify(response);
          console.log("Client connection: " + str);
          sessionStorage.setItem("clico", str);
          sessionStorage.removeItem("commande");
          this.logServ.sendUpdate();
        }

      }
      ,
      err => {
        this.message_co = "Erreur authentification";
      }
    );




  }



}
