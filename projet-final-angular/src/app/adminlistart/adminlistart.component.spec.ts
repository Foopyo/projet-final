import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminlistartComponent } from './adminlistart.component';

describe('AdminlistartComponent', () => {
  let component: AdminlistartComponent;
  let fixture: ComponentFixture<AdminlistartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminlistartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminlistartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
