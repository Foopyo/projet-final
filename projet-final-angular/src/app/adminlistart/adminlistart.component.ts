import { Component, OnInit } from '@angular/core';
import { SrvCrudService } from '../srv-crud.service';

import { Article2 } from '../article2';

@Component({
  selector: 'app-adminlistart',
  templateUrl: './adminlistart.component.html',
  styleUrls: ['./adminlistart.component.css']
})
export class AdminlistartComponent implements OnInit {

  ListArtAdm : Array<Article2>;
  newart={nom:'', description:'', prix:0, url_image:'' };
  message_lstart:string;
  

  constructor(private srv : SrvCrudService) { }

  ngOnInit(): void {
this.init();

  }


  modifart(artmodif){
    if(artmodif.nom==''||artmodif.description==''||artmodif.url_image==''){
      this.message_lstart="Merci de remplir tous les champs";
    }
    else{
      
    this.srv.updateArticle2(artmodif,this.init.bind(this));
    this.init();
    this.message_lstart="L'article a bien été modifié";
    
    }

  }

 
  supprart(id){
    this.srv.deleteArticle2(id,this.init.bind(this));
    this.message_lstart="Article supprimé";
    this.init();

  }

  
  
  ajoutart(){
    if(this.newart.nom==''||this.newart.description==''||this.newart.url_image==''){
      this.message_lstart="Merci de remplir tous les champs";
    }
    else{
      
      this.srv.createArticle2(this.newart,this.init.bind(this));
      this.message_lstart="Article ajouté";
    }
    
  //  this.init();
  }


  init(){

    //console.log("rentre init");
    this.srv.getListeArticle2(this.getart.bind(this));
    ;
   

  }

  getart(){
    this.ListArtAdm=JSON.parse(sessionStorage.getItem("liste_article"));
    console.log("recup art");

    this.newart.nom='';
    this.newart.description='';
    this.newart.prix=0;
    this.newart.url_image='';
    

  }

}
