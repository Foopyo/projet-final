import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResaClientComponent } from './resa-client.component';

describe('ResaClientComponent', () => {
  let component: ResaClientComponent;
  let fixture: ComponentFixture<ResaClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResaClientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResaClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
