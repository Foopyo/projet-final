import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from '../client';
import { Resa } from '../resa';

@Component({
  selector: 'app-resa-client',
  templateUrl: './resa-client.component.html',
  styleUrls: ['./resa-client.component.css']
})
export class ResaClientComponent implements OnInit {

  ListResaCli: Array<Resa>;

  constructor(private router: Router, private http:HttpClient) { }

  ngOnInit(): void {
    this.initresa();
  }

  initresa() {

    //on va chercher la liste des résa:
    console.log("init resas");
    let c:Client = JSON.parse(sessionStorage.getItem("clico"));

    this.http.get<Array<Resa>>("http://localhost:8080/projetFinal/reservations/idcli/" + c.id).subscribe(
      response => {
        this.ListResaCli = response;
        console.log("init resas 2");
        ;

      }
      ,
      err => {
        console.log("init resas ERREUR");
      }
    );

  }

  Annulerresa(idres){
    console.log("suppr resa 1");
    console.log("", idres);

    this.http.delete("http://localhost:8080/projetFinal/reservations/"+idres).subscribe(
      response => {
        console.log("résa supprimé");
        this.initresa();

      },
      err => {
        console.log("erreur suppression résa");
      }
    );

  }


  Newresa(){
    this.router.navigate(['/reservation']);
  }
}
