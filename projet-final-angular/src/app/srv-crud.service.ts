import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Article } from './article';
import { Article2 } from './article2';
import { Ligne } from './ligne';
import { Commande } from './commande';
import { Client } from './client';
import { LigneCommande } from './ligne-commande';

@Injectable({
  providedIn: 'root'
})
export class SrvCrudService {

  article: any;

  constructor(private http: HttpClient) { }

  createCommande(date: string) {

    let c: Commande = new Commande();
    c = JSON.parse(sessionStorage.getItem("commande"));

    let cl: Client = new Client();
    cl = JSON.parse(sessionStorage.getItem("clico"));

    let lst: LigneCommande = new LigneCommande();
    lst.client=cl;
    lst.date=date;
    lst.lignes=c.listLigne;

    console.log(JSON.stringify(lst));

    const body = JSON.stringify(lst);
    this.http.post("http://localhost:8080/projetFinal/commandes", body, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(
      response => {
        console.log("commande créé");
      },
      err => {
        console.log("erreur création commande");
      }
    );
  }

  getListeArticle() {
    this.http.get<Array<Article>>("http://localhost:8080/projetFinal/articles").subscribe(
      response => {
        sessionStorage.setItem("liste_article", JSON.stringify(response));
      }
      ,
      err => {
        console.log(err)
      }
    );
  }

  createArticle2(data, callback:Function) {
    const body = JSON.stringify(data);
    this.http.post("http://localhost:8080/projetFinal/articles/", body, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(
      response => {
        callback();
        console.log("article créé");
      },
      err => {
        console.log("erreur création article");
      }
    );
  }

  deleteArticle2(id, callback:Function) {
   // const body = JSON.stringify(data);
    this.http.delete("http://localhost:8080/projetFinal/articles/"+id).subscribe(
      response => {
        console.log("article supprimé");
        callback();
      },
      err => {
        console.log("erreur suppression article");
      }
    );
  }
  getListeArticle2(callback:Function) {
    this.http.get<Array<Article2>>("http://localhost:8080/projetFinal/articles").subscribe(
      response => {
        sessionStorage.setItem("liste_article", JSON.stringify(response));
        callback();
      }
      ,
      err => {
        console.log(err)
      }
    );
  }

  getArticleById(id: number, callback: Function, qtt: number, lst: Array<Ligne>, prix: number) {
    this.http.get("http://localhost:8080/projetFinal/articles/" + id).subscribe(
      response => {
        sessionStorage.setItem("article", JSON.stringify(response));
        callback(qtt, lst, prix);
      },
      err => {
        console.log("ID inexistant")
      }
    );
  }

  updateArticle2(data,callback:Function) {

    const body=JSON.stringify(data);

   
    this.http.post("http://localhost:8080/projetFinal/articles/",body,{
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      callback();
    },

      err => {
       
  
      });
  }

}
