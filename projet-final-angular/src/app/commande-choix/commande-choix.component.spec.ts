import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandeChoixComponent } from './commande-choix.component';

describe('CommandeChoixComponent', () => {
  let component: CommandeChoixComponent;
  let fixture: ComponentFixture<CommandeChoixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommandeChoixComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CommandeChoixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
