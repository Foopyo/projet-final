import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Ligne } from '../ligne';
import { Article } from '../article';
import { Commande } from '../commande';
import { SrvCrudService } from '../srv-crud.service';

@Component({
  selector: 'app-commande-choix',
  templateUrl: './commande-choix.component.html',
  styleUrls: ['./commande-choix.component.css']
})
export class CommandeChoixComponent implements OnInit {

  message:string;
  iscommande:boolean;
  Mylist: any;
  select_choice: any;
  qtt: number = 0;
  prix_commande: number = 0;
  liste_commande: Array<Ligne> = [];
  ligne = { article: new Article(), quantite: 0 };
  select: HTMLSelectElement;

  constructor(private http: HttpClient, private route: Router, private srv: SrvCrudService) { }

  ngOnInit(): void {
    this.srv.getListeArticle();
    this.Mylist = JSON.parse(sessionStorage.getItem("liste_article"));
    this.select = (document.getElementById("plat")) as HTMLSelectElement;

    let c: Commande = new Commande();
    c = JSON.parse(sessionStorage.getItem("commande"));

    this.liste_commande = c.listLigne;
    this.prix_commande = c.prixGlobal;

    if (this.liste_commande.length!=0) {
      this.iscommande=true;
    }
  }

  ajout() {
    let selectedOption = this.select.selectedIndex;
    let a = this.Mylist[selectedOption];

    if (a != null) {
      if (this.qtt > 0) {
        let l: Ligne = new Ligne();
        let prix_ligne: number;
        let isnew: boolean = true;
        this.liste_commande.forEach(element => {
          if (element.article.id == a.id) {
            element.quantite += this.qtt;
            element.prixLigne += element.article.prix * this.qtt;
            prix_ligne = element.article.prix * this.qtt;
            isnew = false;
          }
        });

        if (isnew) {
          l.article = a;
          l.quantite = this.qtt;
          l.prixLigne = l.quantite * (a.prix);
          this.liste_commande.push(l);
          prix_ligne = l.prixLigne;
        }

        this.prix_commande += prix_ligne;

        let c: Commande = new Commande();
        c.listLigne = this.liste_commande;
        c.prixGlobal = this.prix_commande;

        let str: string = JSON.stringify(c);
        sessionStorage.setItem("commande", str);

        this.iscommande=true;
      } else {
        this.message = "Veuillez choisir une quantité positive";
      }
    } else {
      this.message = "Veuillez choisir un article.";
    }
  }

  supprimer(ligne: Ligne) {
    const index = this.liste_commande.indexOf(ligne);
    if (index > -1) {
      let c: Commande = new Commande();
      this.prix_commande -= ligne.prixLigne;
      c.prixGlobal = this.prix_commande;

      this.liste_commande.splice(index, 1);
      c.listLigne = this.liste_commande;

      let str: string = JSON.stringify(c);

      sessionStorage.setItem("commande", str);

    }
    if (this.liste_commande.length==0) {
      this.iscommande=false;
    }
  }

  vider() {

    let c: Commande = new Commande();
    this.liste_commande.splice(0);
    c.listLigne = this.liste_commande;
    this.prix_commande = 0;
    c.prixGlobal = this.prix_commande;

    let str: string = JSON.stringify(c);
    sessionStorage.setItem("commande", str);

    this.iscommande=false;
  }

  continuer() {
    this.route.navigate(['commanderecap']);
  }
}
