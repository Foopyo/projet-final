import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from '../client';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-inscriptionclient',
  templateUrl: './inscriptionclient.component.html',
  styleUrls: ['./inscriptionclient.component.css']
})
export class InscriptionclientComponent implements OnInit {
  log2: string;
  message_dispo_log: string;
  cli1: Client;
  cli2: Client;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    //ici on redirige vers la page utilisateur si on est déjà connecté:
    this.cli1 = JSON.parse(sessionStorage.getItem("clico"));
    if (this.cli1 != null) {
      this.router.navigate(['/pageutilisateur']);
    }
  }

 
  verif_dispo_log(){

    if(this.log2==null){
this.message_dispo_log="Veuillez entrer un login";
    }

    else{

    this.http.get<Client>("http://localhost:8080/projetFinal/clients/searchLogin/"+this.log2).subscribe(
      response => {
        this.cli2 = response;
        if (this.cli2 == null) {
          sessionStorage.setItem("loginscr", this.log2);
          this.message_dispo_log = "";
          this.router.navigate(['/inscriptionclient2']);
        }
        else {
          this.message_dispo_log = "Login déjà utilisé, veuillez en choisir un autre";
        }
      }
      ,
      err => {
        this.message_dispo_log = "Erreur";
      }
    );
  

    
  }
    
  }
}
