import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { Client } from "./client";

@Injectable({providedIn: 'root'})
export class LoginService {
  private subjectName = new Subject<any>();

  sendUpdate(){
    this.subjectName.next({});
  }

  getUpdate():Observable<any>{
    return this.subjectName.asObservable();
  }
}
