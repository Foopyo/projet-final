import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login-service';

@Component({
  selector: 'app-deconnection',
  templateUrl: './deconnection.component.html',
  styleUrls: ['./deconnection.component.css']
})
export class DeconnectionComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
    
  }

}
