import { Component, OnInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Client } from '../client';
import { LoginService } from '../login-service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  isclient:boolean;
  isadmin:boolean;
  private subscriptionName: Subscription
  accueilTab:HTMLElement;
  resaTab:HTMLElement;
  carteTab:HTMLElement;
  adminTab:HTMLElement;
  adminResaTab:HTMLElement;
  commandeTab:HTMLElement;
  connectionTab:HTMLElement;
  inscriptionTab:HTMLElement;
  compteTab:HTMLElement;
  currentTab:HTMLElement;

  constructor(private route: Router, private logServ: LoginService) {
    this.subscriptionName = this.logServ.getUpdate().subscribe(
      ()=>{this.setClient();this.pageUtilisateur();}
    )
  }

  ngOnInit(): void {
    this.setClient();
    this.accueilTab = document.getElementById("accueil");
    this.resaTab = document.getElementById("resa");
    this.carteTab = document.getElementById("carte");
    this.adminTab = document.getElementById("admin");
    this.adminResaTab = document.getElementById("adminResa");
    this.commandeTab = document.getElementById("commande");
    this.connectionTab = document.getElementById("connexion");
    this.inscriptionTab = document.getElementById("inscription");
    this.compteTab = document.getElementById("compte");
  }

  setCurrentTab(tab:HTMLElement){
    if(this.currentTab != undefined)
      this.currentTab.classList.remove("active");
    tab.classList.add("active");
    this.currentTab = tab;
  }

  setClient(){
    let cl:Client = JSON.parse(sessionStorage.getItem("clico"));
    this.isclient = cl != null;
    this.isadmin = this.isclient && cl.admin;
  }

  accueil(){
    this.setCurrentTab(this.accueilTab);
    this.route.navigate(['accueil']);
  }

  carte(){
    this.setCurrentTab(this.carteTab);
    this.route.navigate(['carte']);
  }

  reservation(){
    this.setCurrentTab(this.resaTab);
    this.route.navigate(['reservation']);
  }

  admin(){
    this.setCurrentTab(this.adminTab);
    this.route.navigate(['adminlistart']);
  }

  adminResa(){
    this.setCurrentTab(this.adminResaTab);
    this.route.navigate(['adminresa']);
  }

  connexion(){
    this.setCurrentTab(this.connectionTab);
    this.route.navigate(['connectionclient']);
  }

  inscription(){
    this.setCurrentTab(this.inscriptionTab);
    this.route.navigate(['inscriptionclient']);
  }

  commande(){
    this.setCurrentTab(this.commandeTab);
    this.route.navigate(['commandechoix']);
  }

  pageUtilisateur(){
    this.setCurrentTab(this.compteTab);
    this.route.navigate(['pageutilisateur']);
  }

  deconnexion(){
    this.setCurrentTab(this.accueilTab);
    sessionStorage.removeItem("clico");
    this.setClient();
    this.route.navigate(['accueil']);
  }

  historiquecommande(){
    this.setCurrentTab(this.compteTab);
    this.route.navigate(['historiquecommande']);
  }

  historiquereservation(){
    this.setCurrentTab(this.compteTab);
    this.route.navigate(['historiquereservation']);
  }
}
