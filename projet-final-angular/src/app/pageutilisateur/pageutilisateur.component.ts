import { Component, OnInit } from '@angular/core';
import { Client } from '../client';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Resa } from '../resa';

@Component({
  selector: 'app-pageutilisateur',
  templateUrl: './pageutilisateur.component.html',
  styleUrls: ['./pageutilisateur.component.css']
})
export class PageutilisateurComponent implements OnInit {

  message_update: String;
  cli1: Client;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.init();
  }
  init(){
    this.cli1 = JSON.parse(sessionStorage.getItem("clico"));

    if (this.cli1 == null) {
      this.router.navigate(['/connectionclient']);
    }

    console.log(sessionStorage.getItem("clico"));

    //cette étape est nécessaire, sous peine de ne pouvoir modifier deux fois de suite (cf version non actualisée):

    this.http.get<Client>("http://localhost:8080/projetFinal/clients/" + this.cli1.id).subscribe(
      response => {
        this.cli1 = response;
      }
      ,
      err => {}
    );
  }

  modifinfo() {

    if(this.cli1.nom==''||this.cli1.psw==''||this.cli1.adresse==''||this.cli1.tel==''||this.cli1.prenom==''){
      this.message_update="Merci de compléter tous les champs";
    }
    else{

    
    this.message_update="";
    const body = JSON.stringify(this.cli1);
    console.log(body);
    this.http.post("http://localhost:8080/projetFinal/clients/", body, {

      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      sessionStorage.setItem("clico", body);
      this.message_update="Vos données ont bien été modifiées";
      this.init();
     // console.log(sessionStorage.getItem("clico"));
    },
      err => {
        this.message_update = "Erreur dans la modification de vos données"
      });

  }
}
}
