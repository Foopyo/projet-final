export class Client {

    id: number;
    login: string;
    psw: string;
    nom: string;
    prenom: string;
    adresse: string;
    tel: string;
    admin: boolean;
}
