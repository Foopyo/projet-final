import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppHelloComponent } from './app-hello.component';

describe('AppHelloComponent', () => {
  let component: AppHelloComponent;
  let fixture: ComponentFixture<AppHelloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppHelloComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppHelloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
