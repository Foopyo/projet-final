import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppHelloComponent } from './app-hello/app-hello.component';
import { CommandeChoixComponent } from './commande-choix/commande-choix.component';
import { CommandeRecapComponent } from './commande-recap/commande-recap.component';
import { CommandeInfoComponent } from './commande-info/commande-info.component';
import { AccueilComponent } from './accueil/accueil.component';
import { CarteComponent } from './carte/carte.component';
import { ConnectionclientComponent } from './connectionclient/connectionclient.component';
import { InscriptionclientComponent } from './inscriptionclient/inscriptionclient.component';
import { Inscriptionclient2Component } from './inscriptionclient2/inscriptionclient2.component';
import { PageutilisateurComponent } from './pageutilisateur/pageutilisateur.component';
import { DeconnectionComponent } from './deconnection/deconnection.component';
import { AdminlistartComponent } from './adminlistart/adminlistart.component';
import { ReservationComponent } from './reservation/reservation.component';
import { AdminresaComponent } from './adminresa/adminresa.component';
import { ResaClientComponent } from './resa-client/resa-client.component';
import { HistoriquecommandeComponent } from './historiquecommande/historiquecommande.component';

const routes: Routes = [
  { path:'app1', component: AppHelloComponent},
  { path:'accueil', component: AccueilComponent},
  { path:'carte', component: CarteComponent},
  { path:'commandechoix', component: CommandeChoixComponent},
  { path:'commanderecap', component: CommandeRecapComponent},
  { path:'commandeinfo', component: CommandeInfoComponent},
  { path:'connectionclient', component: ConnectionclientComponent},
  { path:'inscriptionclient', component: InscriptionclientComponent},
  { path:'inscriptionclient2', component: Inscriptionclient2Component},
  { path:'pageutilisateur', component: PageutilisateurComponent},
  { path:'deconnection', component: DeconnectionComponent},
  { path:'adminlistart', component: AdminlistartComponent},
  { path:'adminresa', component: AdminresaComponent},
  { path:'historiquereservation', component: ResaClientComponent},
  { path:'reservation', component: ReservationComponent}, 
  { path:'historiquecommande', component: HistoriquecommandeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
