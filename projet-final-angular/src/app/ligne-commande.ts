import { Client } from './client';
import { Article } from './article';
import { Ligne } from './ligne';

export class LigneCommande {
    date:string;
    client:Client;
    lignes:Array<Ligne>;

    constructor(){}
}
