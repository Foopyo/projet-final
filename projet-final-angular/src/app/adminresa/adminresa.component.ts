import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Resa } from '../resa';

@Component({
  selector: 'app-adminresa',
  templateUrl: './adminresa.component.html',
  styleUrls: ['./adminresa.component.css']
})
export class AdminresaComponent implements OnInit {

  listResa:Array<Resa>;

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    this.initListResa();
  }

  initListResa() {
    this.http.get<Array<Resa>>("http://localhost:8080/projetFinal/reservations/allbyjour").subscribe(
      response => {
        this.listResa = response;
      }
      ,
      err => {
        console.log("init resas ERREUR");
      }
    );
  }

  cancelResa(id:number){
    this.http.delete("http://localhost:8080/projetFinal/reservations/"+id).subscribe(
      response => {
        console.log("résa supprimé");
        this.initListResa();
      },
      err => {
        console.log("erreur suppression résa");
      }
    );
  }

}
