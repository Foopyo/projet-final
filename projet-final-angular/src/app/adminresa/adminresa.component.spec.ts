import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminresaComponent } from './adminresa.component';

describe('AdminresaComponent', () => {
  let component: AdminresaComponent;
  let fixture: ComponentFixture<AdminresaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminresaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
