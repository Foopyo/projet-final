import { Component, OnInit } from '@angular/core';
import { Commande } from '../commande';
import { Client } from '../client';

@Component({
  selector: 'app-commande-info',
  templateUrl: './commande-info.component.html',
  styleUrls: ['./commande-info.component.css']
})
export class CommandeInfoComponent implements OnInit {

  prix_commande:number;
  client:Client;

  constructor() { }

  ngOnInit(): void {
    let c: Commande = new Commande();
    c=JSON.parse(sessionStorage.getItem("commande"));
    this.prix_commande=c.prixGlobal;

    let cl: Client = new Client();
    cl=JSON.parse(sessionStorage.getItem("clico"));
    this.client=cl;

    sessionStorage.removeItem("commande");
  }

}
