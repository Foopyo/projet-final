package cgi.project.ProjetFinal.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cgi.project.ProjetFinal.model.Article;
import cgi.project.ProjetFinal.repo.ArticleRepository;

@RestController
@RequestMapping("/articles")
public class ArticleRestController {
	
	@Autowired
	private ArticleRepository repo;

	@CrossOrigin
	@GetMapping
	public List<Article> findAll() {
		return repo.findAll();
	}

	@CrossOrigin
	@GetMapping("{id}")
	public Article findByID(@PathVariable(name = "id") int id) {
		return repo.findById(id).get();
	}

	@CrossOrigin
	@PostMapping
	public void create(@RequestBody Article a) {
		repo.save(a);
	}

	@CrossOrigin
	@DeleteMapping("{id}")
	public void delete(@PathVariable(name = "id") int id) {
		repo.delete(repo.findById(id).get());
	}

	@CrossOrigin
	@PutMapping
	public void update(@RequestBody Article a) {
		Optional<Article> article = repo.findById(a.getId());
		if(article.isPresent()){
			a.setVersion(article.get().getVersion());
			repo.save(a);
		}
	}

	@CrossOrigin
	@GetMapping("search/{nom}")
	public List<Article> searchArticle(@PathVariable(name = "nom") String nom) {
		List<Article> articles=repo.findByNomContains(nom);
		try{
			articles.add(repo.findById(Integer.parseInt(nom)).get());
		}finally{
			return articles;
		}
	}
}
