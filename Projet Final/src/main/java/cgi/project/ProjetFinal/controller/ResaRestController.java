package cgi.project.ProjetFinal.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cgi.project.ProjetFinal.model.Client;
import cgi.project.ProjetFinal.model.Resa;
import cgi.project.ProjetFinal.repo.ResaRepository;

@RestController
@RequestMapping("/reservations")
public class ResaRestController {
	
	@Autowired
	private ResaRepository repo;
	
	@CrossOrigin
	@GetMapping
	public List<Resa> findAll() {
		return repo.findAll();
	}

	@CrossOrigin
	@PostMapping
	public void create(@RequestBody Resa c) {
		repo.save(c);
	}

	@CrossOrigin
	@DeleteMapping("{id}")
	public void delete(@PathVariable(name = "id") int id) {
		repo.delete(repo.findById(id).get());
	}

	@CrossOrigin
	@PutMapping
	public void update(@RequestBody Resa c) {
		Optional<Resa> res = repo.findById(c.getIdresa());
		if(res.isPresent()){
			c.setVersion(res.get().getVersion());
			repo.save(c);
		}
	}
	
	
	@CrossOrigin
	@GetMapping("idcli/{idclient}")
	public List<Resa> findByIdclient(@PathVariable(name = "idclient") int idclient) {
		return repo.findByIdclientOrderByJourNumber(idclient);
	}
	
	@CrossOrigin
	@GetMapping("allbyjour")
	public List<Resa> findAllByOrderByJourAsc2() {
		return repo.findAllByOrderByJourNumberAscHeureNumberAsc();
	}

}
