package cgi.project.ProjetFinal.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cgi.project.ProjetFinal.model.Client;
import cgi.project.ProjetFinal.model.Commande;
import cgi.project.ProjetFinal.model.LigneCommande;
import cgi.project.ProjetFinal.repo.CommandeRepository;
import cgi.project.ProjetFinal.repo.LigneCommandeRepository;

@RestController
@RequestMapping("/commandes")
public class CommandeRestController {
	@Autowired
	private CommandeRepository repoCommande;
	@Autowired
	private LigneCommandeRepository repoLigne;

	@CrossOrigin
	@GetMapping
	public List<Commande> findAll() {
		return repoCommande.findAll();
	}

	@CrossOrigin
	@PostMapping
	public void create(@RequestBody Commande c) {
		List<LigneCommande> lignes  = c.getLignes();
		c.setLignes(null);
		repoCommande.save(c);
		for(LigneCommande l : lignes)
			l.setCommande(c);
		repoLigne.saveAll(lignes);
		c.setLignes(lignes);
		repoCommande.save(c);
	}

	@CrossOrigin
	@DeleteMapping("{id}")
	public void delete(@PathVariable(name = "id") int id) {
		repoCommande.delete(repoCommande.findById(id).get());
	}

	@CrossOrigin
	@PutMapping
	public void update(@RequestBody Commande c) {
		Optional<Commande> commande = repoCommande.findById(c.getId());
		if(commande.isPresent()){
			c.setVersion(commande.get().getVersion());
			repoCommande.save(c);
		}
	}

	@CrossOrigin
	@PostMapping("/searchByClient")
	public List<Commande> searchByClient(@RequestBody Client c) {
		return repoCommande.findByClientOrderByDateDesc(c);
	}
}
