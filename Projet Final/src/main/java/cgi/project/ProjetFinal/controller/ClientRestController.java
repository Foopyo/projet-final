package cgi.project.ProjetFinal.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cgi.project.ProjetFinal.model.Client;
import cgi.project.ProjetFinal.repo.ClientRepository;

@RestController
@RequestMapping("/clients")
public class ClientRestController {

	@Autowired
	private ClientRepository repo;

	@CrossOrigin
	@GetMapping
	public List<Client> findall() {
		return repo.findAll();
	}

	@CrossOrigin
	@GetMapping("{id}")
	public Client findbyid(@PathVariable(name = "id") int id) {
		return repo.findById(id).get();
	}

	@CrossOrigin
	@PostMapping
	public void create(@RequestBody Client p) {
		repo.save(p);
	}

	@CrossOrigin
	@DeleteMapping("{id}")
	public void delete(@PathVariable(name = "id") int id) {
		repo.delete(repo.findById(id).get());
	}

	@CrossOrigin
	@PutMapping
	public void update(@RequestBody Client c) {
		Optional<Client> client = repo.findById(c.getId());
		if(client.isPresent()){
			c.setVersion(client.get().getVersion());
			repo.save(c);
		}
	}

	@CrossOrigin
	@GetMapping("{login}/{psw}")
	public Client findByLoginAndPsw(@PathVariable(name = "login") String login,
			@PathVariable(name = "psw") String psw) {
		Client p = repo.findByLoginAndPsw(login, psw);
		return p;
	}
	
	@CrossOrigin
	@GetMapping("searchLogin/{login}")
	public Client findByLoginAndPsw(@PathVariable(name = "login") String login) {
		return repo.findByLogin(login);
	}

}
