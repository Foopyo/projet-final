package cgi.project.ProjetFinal.repo;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cgi.project.ProjetFinal.model.Resa;

public interface ResaRepository extends JpaRepository<Resa,Integer> {
	public List<Resa> findByIdclientOrderByJourNumber(int idcli);
	public List<Resa> findAllByOrderByJourNumberAscHeureNumberAsc();
}
