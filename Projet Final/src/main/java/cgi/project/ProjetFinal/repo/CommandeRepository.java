package cgi.project.ProjetFinal.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cgi.project.ProjetFinal.model.Client;
import cgi.project.ProjetFinal.model.Commande;

public interface CommandeRepository extends JpaRepository<Commande, Integer>{
	public List<Commande> findByClientOrderByDateDesc(Client client);
}
