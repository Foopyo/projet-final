package cgi.project.ProjetFinal.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import cgi.project.ProjetFinal.model.Client;

public interface ClientRepository extends JpaRepository<Client,Integer>{
	
	public Client findByLoginAndPsw(String log, String mdp);
	
	public Client findByLogin(String log);

}
