package cgi.project.ProjetFinal.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import cgi.project.ProjetFinal.model.LigneCommande;
import cgi.project.ProjetFinal.model.LigneCommandePK;

public interface LigneCommandeRepository extends JpaRepository<LigneCommande, LigneCommandePK>{

}
