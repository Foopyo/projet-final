package cgi.project.ProjetFinal.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cgi.project.ProjetFinal.model.Article;

public interface ArticleRepository extends JpaRepository<Article, Integer>{
	public List<Article> findByNomContains(String nom);
}
