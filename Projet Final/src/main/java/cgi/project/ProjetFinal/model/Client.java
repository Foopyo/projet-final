package cgi.project.ProjetFinal.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Client {

	private int id, version;
	private String login, nom, prenom, adresse, tel, psw;
	private boolean admin;
	private List<Commande> commandes = new ArrayList<Commande>();

	public Client(String login, String nom, String prenom) {
		this(-1, login, nom, prenom, null, null);
	}

	public Client(int id, String login, String nom, String prenom) {
		this(id, login, nom, prenom, null, null);
	}

	public Client(String login, String nom, String prenom, String adresse, String tel) {
		this(-1, login, nom, prenom, adresse, tel);
	}

	public Client() {

	}

	public Client(int id, String login, String nom, String prenom, String adresse, String tel) {
		this.id = id;
		this.login = login;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.tel = tel;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	@Version
	public int getVersion() {
		return this.version;
	}

	public String getLogin() {
		return login;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public String getTel() {
		return tel;
	}

	public String getPsw() {
		return psw;
	}

	public boolean isAdmin() {
		return admin;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "client", cascade = { CascadeType.ALL })
	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public void setPsw(String psw) {
		this.psw = psw;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", login=" + login + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse
				+ ", tel=" + tel + ", admin=" + admin + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
