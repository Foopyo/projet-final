package cgi.project.ProjetFinal.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@IdClass(LigneCommandePK.class)
public class LigneCommande implements Serializable {

	private Commande commande;
	private Article article;
	private int quantite;

	@Id
	@ManyToOne
	@JoinColumn(name = "id_article", referencedColumnName = "id")
	public Article getArticle() {
		return article;
	}

	public int getQuantite() {
		return quantite;
	}

	@Id
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "id_commande", referencedColumnName = "id")
	@JsonIgnore
	public Commande getCommande() {
		return commande;
	}

	public void setCommande(Commande commande) {
		this.commande = commande;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
}
