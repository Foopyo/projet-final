package cgi.project.ProjetFinal.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity
public class Commande {
	private int id, version;
	private Date date;
	private Client client;
	private List<LigneCommande> lignes;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	@Version
	public int getVersion() {
		return version;
	}

	public Date getDate() {
		return date;
	}

	@ManyToOne
	public Client getClient() {
		return client;
	}

	@OneToMany(mappedBy="commande", cascade = { CascadeType.ALL })
	public List<LigneCommande> getLignes() {
		return lignes;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public void setLignes(List<LigneCommande> lignes) {
		this.lignes = lignes;
	}

}
