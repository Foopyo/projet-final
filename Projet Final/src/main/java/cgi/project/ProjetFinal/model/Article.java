package cgi.project.ProjetFinal.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Article {
	private int id, version;
	private double prix;
	private String nom, description, url_image;
	private List<LigneCommande> commandes;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	@Version
	public int getVersion() {
		return version;
	}

	public double getPrix() {
		return prix;
	}

	public String getNom() {
		return nom;
	}

	public String getDescription() {
		return description;
	}

	public String getUrl_image() {
		return url_image;
	}

	@JsonIgnore
	@OneToMany(mappedBy = "article", cascade = { CascadeType.ALL })
	public List<LigneCommande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<LigneCommande> commandes) {
		this.commandes = commandes;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUrl_image(String url_image) {
		this.url_image = url_image;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Article other = (Article) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
