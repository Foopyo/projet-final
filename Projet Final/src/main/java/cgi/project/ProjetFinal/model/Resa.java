package cgi.project.ProjetFinal.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity
public class Resa {
	private int idresa;
	private String jour;
	private String heure;
	private String nom;
	private String tel;
	private int idclient;
	private int nbrpers;
	private int jourNumber;
	private int heureNumber;
	private int version;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getIdresa() {
		return idresa;
	}
	public void setIdresa(int idresa) {
		this.idresa = idresa;
	}
	public String getJour() {
		return jour;
	}
	public void setJour(String jour) {
		this.jour = jour;
	}
	public String getHeure() {
		return heure;
	}
	public void setHeure(String heure) {
		this.heure = heure;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public int getIdclient() {
		return idclient;
	}
	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}
	public int getNbrpers() {
		return nbrpers;
	}
	public void setNbrpers(int nbrpers) {
		this.nbrpers = nbrpers;
	}
	
	public int getJourNumber() {
		return jourNumber;
	}
	public void setJourNumber(int jourNumber) {
		this.jourNumber = jourNumber;
	}
	
	public int getHeureNumber() {
		return heureNumber;
	}
	public void setHeureNumber(int heureNumber) {
		this.heureNumber = heureNumber;
	}
	@Version
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	
	
	
	
	
	
	

}
