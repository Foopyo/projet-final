package cgi.project.ProjetFinal.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class LigneCommandePK implements Serializable{
	private int commande, article;

	
	public int getCommande() {
		return commande;
	}

	public int getArticle() {
		return article;
	}

	public void setCommande(int commande) {
		this.commande = commande;
	}

	public void setArticle(int article) {
		this.article = article;
	}
}
