# Projet Final

## Diapo

Il y a un PowerPoint dans le répo mais la diapo originale est sur Google Slides: [Diapo originale](https://docs.google.com/presentation/d/1YlrQMmiBIHkMy1yGPr8hXfetL2ucHSOTZAvSa6knUyE/edit?usp=sharing).

## BDD

- Host une BDD sur le port 3306 de votre machine
- Créer un scheme nommé achat-cgi
- serverTimezone=UTC

## Admin

Il est impossible de créer un admin dans le site en lui-même. La seule solution est de mofidier la valeur "admin" d'un utilisateur directement dans la base de données ou en passant par le service REST.